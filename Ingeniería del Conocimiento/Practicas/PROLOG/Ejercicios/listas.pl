ultimo(X,[X]):-!.
ultimo(X,[H|T]):-ultimo(X,T).

longitud(L,[X]):-L is 1,!.
longitud(L,[X|Y]):-longitud(T,Y),L is (T+1).

miembro(X,[X|_]).
miembro(X,[H|T]):-miembro(X,T).
subconjunto([],L).
subconjunto([A],[A]).
subconjunto([A|SC],C):-miembro(A,C),subconjunto(SC,C).