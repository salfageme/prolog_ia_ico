Meta interpretes: una via para la construcción de sistemas expertos (motores de inferencia)
Prolog = pensado en origen para esta tarea; tiene un motor de búsqueda.
Ahora pretendemos, dentro de unas posibilidades, modificar el motor de inferencia para que se adecue a nuestro problema, mediante un metainterprete.
Vamos a tener acceso al proceso de computo del lenguaje y vamos a poder desarrollar IDEs
- Lectura recomendada: Prolog - Brad
* Capitulos de creación de motores de inferencia (puede generar código que incorporar a nuestras aplicaciones reales)

Metainterprete sencillo (sin utilidad)
```
solve(A):-call(A).
% o bien:
solve(A):-A.
```
Metainterprete Vainilla
```
solve(True).
solve(A,B):-solve(A),solve(B).
solve(A):-clause(A,B),solve(B).
```

Existe una interpretación declarativa de este metainterprete:
* Meta vacía: cierta
* Meta conjuntiva: cierta: si se satisface a y b: cierto
* Si solo aparece un argumento `clause/2` localiza una clausula cuya cabeza sea A y devuelve la meta B.
Así mismo podemos hacer una lectura operacional:
* Meta vacía resuelta
* Resolver la meta(A,B) resolver PRIMERO A y despues B (Regla de Cómputo)
* Para resolver la meta(A) selecciona una clausula cuya cabeza unifique con A y resolver con el cuerpo (Regla de búsqueda).

Segunda versión del metainterprete Vainilla
```
solve(true):-!.
% En el metainterprete las variables A y B ya no son objetos concretos sino que se van a instanciar como cláusulas.
solve((A,B)):-!,solve(A),solve(B).
solve(A):-clause(A,B),solve(B).
```
Limita a prolog "puro": evitando reinstanciaciones (backtracking) adectamos directamente a la eficiencia.

### Ejemplo para una base de conocimiento: "Propagación de la señal"

```
valor(w1,1).
conectado(w2,w1).
conectado(w3,w2).

valor(W,X):-conectado(W,V),valor(V,X).
```

Consulta mediante el metainterprete: `?-solve(valor(W,X)).` - Se comprueba que 

### Extensión de Vanilla con predicados predefinidos.

```
builtin(A is B).
builtin(A = B).
...
```

Provocamos la busqueda de cláusulas que no responden a la estructura: cabeza:-cuerpo. si no que se amplía a predicados predefinidos(*builtin*).

Extensión III se Vanilla:
```

```

En todos los interpretes que hagamos a partir de ahora, distinguiremos 2 partes:
* Lenguaje base: 
* Meta-lenguaje: `solve/2` - lenguaje del interprete

Vamos a centrarnos en como modificar el lenguaje base: *Sintactic sugaring* modificar las expresiones para dar un aspecto más comprensible a ese lenguaje base.

* Operador `--->` asignación de un valor.
* Operador `&`: 

op(Precedencia,Asociatividad,Operador).

```
true ---> valor(w1,1).
true ---> conectado(w2,w1).
```

