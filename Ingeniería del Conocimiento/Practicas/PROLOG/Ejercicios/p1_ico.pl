% Práctica 1 - Ingeniería del Conocimiento 
% Samuel Alfageme Sainz

% Base de Conocimiento sobre un árbol genealógico.

% -- Hechos: ----------------
es_padre_de(samuel,juan_carlos).
es_padre_de(estrella,juan_carlos).
es_padre_de(juan_carlos,antonio).
es_padre_de(maria_estrella,amador).
es_padre_de(jesus,amador).
es_madre_de(samuel,maria_estrella).
es_madre_de(estrella,maria_estrella).
es_madre_de(juan_carlos,justina).

% -- Reglas: ----------------
% Reconocemos que un hijo puede ser descendiente por parte de madre o padre:
es_hijo_de(X,Y):-es_padre_de(Y,X);es_madre_de(Y,X).
es_abuelo_de(X,Y):-es_hijo_de(Z,X),es_padre_de(Z,Y).
es_abuela_de(X,Y):-es_hijo_de(Z,X),es_madre_de(Z,Y).
% En este caso X e Y no deben unificar (alguien podria ser su propio hermano)
es_hermano_de(X,Y):-es_padre_de(X,Z),es_padre_de(Y,Z),X\=Y,!.
es_hermano_de(X,Y):-es_madre_de(X,Z),es_madre_de(Y,Z),X\=Y.
% Para estas preguntas nos basta con conocer la primera respuesta cierta:
es_padre(X):-es_padre_de(_,X),!.
es_hermano(X):-es_hermano_de(_,X).

% -- Ejemplo de preguntas: --

% ¿Quién es hijo de amador?
% ? - es_hijo_de(amador,X).
% X = maria_estrella;
% X = jesus.

% ¿Quién es abuelo de estrella?
% ? - es_abuelo_de(estrella,X).
% X = antonio;
% X = amador.

% ¿Es amador padre?
% ? - es_padre(amador).
% true.

% ¿Es samuel hermano? 
% ? - es_hermano(samuel).
% true.

% ¿Quién y de quiénes son abuelas?
% ? - es_abuela_de(X,Y).
% X = samuel,
% Y = justina ;
% X = estrella,
% Y = justina ;
% false.