% Práctica 2 - Ingeniería del Conocimiento 
% Samuel Alfageme Sainz

% Base del conocimiento:
% Periodo de presencia de la dinastía Austria en el trono español.
dinastia(austria,1516,1700).
% Reyes de la casa Austria y sus periodos de reinado.
reina(carlos_i, 1516, 1556).
reina(felipe_ii, 1556, 1598).
reina(felipe_iii, 1598, 1621).
reina(felipe_iv, 1621, 1665).
reina(carlos_ii, 1665, 1700).

% Reglas:

% Comprueba si el año Y pertenece a un periodo dinástico concreto.
pertenece_al_periodo(X,Y):-dinastia(X,L,H),Y>=L,Y=<H.
% Comprueba si el rey X estaba en el trono en el año Y
rey_casa_austria(X,Y):-pertenece_al_periodo(austria,Y),reina(X,L,H),Y>=L,Y=<H.

/* El programa podría extenderse con otros hechos, como los periodos de 
reinado de la dinastía Borbón así como los años de reinado de sus miembros.
pudiendo, gracias al predicado pertenece_al_periodo/2 formular la preguntas
de forma más genérica. */