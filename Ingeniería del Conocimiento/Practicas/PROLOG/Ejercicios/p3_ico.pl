% Práctica 3 - Ingeniería del Conocimiento 
% Samuel Alfageme Sainz

% Base del conocimiento:
% Piezas basicas de la bicicleta:
pieza_basica(llanta).
pieza_basica(radios).
pieza_basica(eje).
pieza_basica(manillar).
pieza_basica(sillin).
pieza_basica(plato).
pieza_basica(pedales).
pieza_basica(pinones).
pieza_basica(cadena).
% Resultados de ensamblar varias piezas básicas o no:
ensamblaje(bicicleta,[rueda_delantera,cuadro,rueda_trasera]).
ensamblaje(rueda_delantera,[llanta,radios,eje]).
ensamblaje(cuadro,[manillar,sillin,traccion]).
ensamblaje(traccion,[eje,plato,pedales,cadena]).
ensamblaje(rueda_trasera,[llanta,radios,eje,pinones]).

% Reglas:
% Obtiene todas las piezas que componen el primer argumento
piezas_de(X,[X]):-pieza_basica(X).
piezas_de(X,L):-ensamblaje(X,M),listarPiezas(M,L).
% Funcion auxiliar: dada una lista de piezas, obtiene todos los nodos hijos
listarPiezas([],[]).
listarPiezas([H|T],L):-piezas_de(H,L1),listarPiezas(T,L2),append(L1,L2,L).