% Práctica 5 - Ingeniería del Conocimiento 
% Samuel Alfageme Sainz
% -----------------------------------------
% METAINTERPRETES : Ejercicio 1
%
% Modificacion del metainterprete vanilla para que utilice la Regla de Computo:
% 					    Primer Literal a la Derecha

solve(true):-!. 
solve((A,B)):-!,solve(B),solve(A).
solve(A):-clause(A,B),solve(B).

valor(w1,1).
conectado(w2,w1).
conectado(w3,w2).
valor(W,X):-conectado(W,V),valor(V,X).

