% Práctica 5 - Ingeniería del Conocimiento 
% Samuel Alfageme Sainz
% -----------------------------------------
% METAINTERPRETES : Ejercicio 4
%
% Modificacion del metainterprete vanilla para que acepte el lenguaje base 
% siguiente: 

/* LENGUAJE BASE DE REEMPLAZO:
 * 
 * true ---> valor(w1,1).
 * true ---> conectado(w2,w1).
 * true ---> conectado(w3,w2).
 *
 * conectado(W,V) & valor(V,X) ---> valor(W,X).
 *
 * DEFINICION DE OPERADORES: predicado op/3
 * :-op(+Precedence, +Type, :Name)
 * Type es uno de {xf, yf, xfx, xfy, yfx, fy , fx} y representa la posicion
 * del functor respecto a los argumentos.
 *
 */

% Nuevos operadores:
:-op(40,xfy,&).
:-op(50,xfy,--->).

% Base de Conocimiento: propagacion de la señal:
true ---> valor(w1,1).
true ---> conectado(w2,w1).
true ---> conectado(w3,w2).
conectado(W,V) & valor(V,X) ---> valor(W,X).

% Metalenguaje:
solve(true):-!.
solve((A & B)) :- !,solve(A),solve(B).
solve(A):- B ---> A,solve(B).