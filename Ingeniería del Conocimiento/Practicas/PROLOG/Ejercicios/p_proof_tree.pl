% Práctica 6 - Ingeniería del Conocimiento 
% Samuel Alfageme Sainz
% -----------------------------------------
% Metainterprete Vanilla con metas diferidas

:-op(40, xfy, &).
:-op(50, xfy, --->).

% +--------------------------------------------------------------+
% |    														     |
% +--------------------------------------------------------------+

hprove(true,true)	  	:- !.
hprove((A & B),(L & R)) :- !,hprove(A,L),hprove(B,R).
hprove(A,if(A,T))       :- (B ---> A),hprove(B,T).

% +--------------------------------------------------------------+
% |                    Base de conocimiento:					 |
% +--------------------------------------------------------------+

% Elementos básicos:
true 			  ---> light(l1).
true 			  ---> light(l2).
true 			  ---> live(outside).
% Estado de los elementos de la red electrica:
true 			  ---> up(s2).
true 			  ---> up(s3).
true 			  ---> down(s1).
% Conexiones directas:
true	          ---> connected_to(w5,outside).
true              ---> connected_to(l1,w0).
true	          ---> connected_to(l2,w4).
true	          ---> connected_to(p1,w3).
true	          ---> connected_to(p2,w6).
% Componentes cuyo funcionamiento es correcto:
true 			  ---> ok(cb1).
true		      ---> ok(cb2).
true 		      ---> ok(s1).
true 			  ---> ok(s2).
true 			  ---> ok(s3).
true		      ---> ok(l1).
true 			  ---> ok(l2).
% Conexiones condicionadas por un interruptor:
ok(cb1)	          ---> connected_to(w3,w5).
ok(cb2)	          ---> connected_to(w6,w5).
% Coneciones condicionadas por un conmutador:
up(s2)   & ok(s2) ---> connected_to(w0,w1).
up(s1)   & ok(s1) ---> connected_to(w1,w3).
up(s3)	 & ok(s3) ---> connected_to(w4,w3).
down(s2) & ok(s2) ---> connected_to(w0,w2).
down(s1) & ok(s1) ---> connected_to(w2,w3).
% Reglas de funcionamiento de la red eléctrica:
light(L) & ok(L) & live(L) 		---> lit(L).
connected_to(W,W1) & live(W1)   ---> live(W).
