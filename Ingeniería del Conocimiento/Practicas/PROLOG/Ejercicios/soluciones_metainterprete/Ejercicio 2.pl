/* 	Meta int�rprete Vanilla	 			*/
/*							*/
/*	2011.						*/
/*	Dpto. de Inform�tica. 				*/
/*	Universidad de Valladolid.			*/
/*							*/
/*	tomado de:					*/
/*	L. Sterling, E. Shapiro				*/
/*	The Art of Prolog, Second Edition		*/
/*	The MIT Press 1994				*/
/*	ISBN 0-262-19338-8				*/





/*	Meta int�rprete Vanilla	 profundidad m�xima	*/
/*							*/





solve_pmax(true, _) :- !.
solve_pmax((A,B), Prf) :- !, solve_pmax(A, Prf), solve_pmax(B, Prf).

/*	Esta es la �nica cl�usula que aumenta la profundidad de la prueba	*/
solve_pmax(A, Prf) :- Prf > 0, clause(A,B), Nprf is Prf-1, solve_pmax(B, Nprf).
	





/* Base de Conocimiento: conexi�n unidireccional  */

valor(w1, 1).
conectado(w2, w1).
conectado(w3, w2).
valor(W,X):-conectado(W,V), valor(V,X).
